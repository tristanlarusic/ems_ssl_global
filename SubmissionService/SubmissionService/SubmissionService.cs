﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.IO;

namespace SubmissionService
{
    public class SubmissionServiceClient : PartyService.NSPartyServicePortTypeClient
    {
        static public void Main(String[] args)
        {
            // string partyReference = "000003365"; // For testing
            string partyReference = GetPartyFromXML(Properties.Resources.SamplePartyFilingSubmissionNotification); // 000003365

            // Parameters
            bool validateOnly = false;
            string originatorId = "test";
            string originatorContext = "test";
            string originatorTrackingId = "test123";

            try
            {
                PartyService.ContextHeaderType contextHeader = new PartyService.ContextHeaderType
                {
                    OriginatorId = originatorId,
                    OriginatorContext = originatorContext,
                    OriginatorTrackingId = originatorTrackingId
                };

                // Define Request Type
                PartyService.GetPartyProfileRequestType getPartyProfileRequestType = new PartyService.GetPartyProfileRequestType
                {
                    validateOnly = validateOnly,
                    ContextHeader = contextHeader,
                    RegisteredEntityPk = partyReference
                };

                PartyService.NSPartyServicePortTypeClient client = new PartyService.NSPartyServicePortTypeClient();

                PartyService.GetPartyProfileResponseType response = client.GetPartyProfile(getPartyProfileRequestType);

                Console.WriteLine("**************");
                Console.WriteLine("Status: " + response.ResponseStatus);
                Console.WriteLine("**************");
                Console.WriteLine("Message: " + response.ResultMessages);
                Console.WriteLine("**************");
                Console.WriteLine("Party Profile Details: " + response.PartyProfileDetails);
                Console.WriteLine("**************");
                Console.ReadKey();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        public static string GetPartyFromXML(string xml)
        {
            Regex rx = new Regex(@"<Party>(\d+)<\/Party>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Match partyId = rx.Match(xml);

            return partyId.Groups[1].Value;
        }

        /*
        // *****************************
        // Below uses old method of manually creating envelope
        // *****************************
        public static void callWebService(string partyId)
        {
            var _url = "https://sit.nsrpcsa.ca:8443/SOAPServiceBinding/NSPartyServicePortType";
            var _action = "https://sit.nsrpcsa.ca:8443/SOAPServiceBinding/NSPartyServicePortType?wsdl";

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(partyId);
            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete..
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            string soapResult;
            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }
                Console.Write(soapResult);
            }
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static XmlDocument CreateSoapEnvelope(string partyID)
        {
            bool validateOnly = false;
            string originatorId = "test";
            string originatorContext = "test";
            string originatorTrackingId = "test123";

            XmlDocument soapEnvelopeDocument = new XmlDocument();
            soapEnvelopeDocument.LoadXml(
                $@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:urn=""urn:com.cgi.party.partyservice.v1"" xmlns:urn1=""urn:com.cgi.common.service.types.v1"" xmlns:urn2=""urn:com.cgi.common.types.v1"">
                    < soapenv:Header />
                    < soapenv:Body >
                        < urn:GetPartyProfileRequest validateOnly = ""{validateOnly}"" timezone = ""+00:00"" >
                            < urn1:ContextHeader >
                                < urn2:OriginatorId > {originatorId} </ urn2:OriginatorId >
                                < urn2:OriginatorContext > {originatorContext} </ urn2:OriginatorContext >
                                < urn2:OriginatorTrackingId > {originatorTrackingId} </ urn2:OriginatorTrackingId >
                                < urn2:RequestSentTime > {DateTime.Now} </ urn2:RequestSentTime >
                             </ urn1:ContextHeader >
                         < urn:Version > 2 </ urn:Version >
                        < urn:RegisteredEntityPk > {partyID} </ urn:RegisteredEntityPk >
                    </ urn:GetPartyProfileRequest >
                </ soapenv:Body >
               </ soapenv:Envelope >
            ");
            return soapEnvelopeDocument;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
        */
    }
}